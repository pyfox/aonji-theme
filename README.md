## Aonji-theme

A dark brownish theme for IntelliJ IDEs with black accents, orange highlights 
and a slight breeze of steampunk.

![Screenshot](https://gitlab.com/pyfox/aonji-theme/-/raw/main/data/screenshot.png)
